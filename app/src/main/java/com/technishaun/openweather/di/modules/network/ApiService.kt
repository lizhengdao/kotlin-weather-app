package com.technishaun.openweather.di.modules.network

import com.technishaun.openweather.objects.items.responsecityweather.City
import com.technishaun.openweather.objects.response.ResponseCitiesWeather
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ApiService {
    @GET("group")
    fun getCitiesWeather(@QueryMap data: HashMap<String, String>): Single<ResponseCitiesWeather>

    @GET("weather")
    fun getCityWeather(@QueryMap data: HashMap<String, String>): Single<City>
}