package com.technishaun.openweather.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.technishaun.openweather.R
import kotlinx.android.synthetic.main.act_main.*

class ActMain : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_main)
        setSupportActionBar(tb_main)
        setTitle(R.string.page_title)

        NavigationUI.setupWithNavController(tb_main, Navigation.findNavController(this, R.id.fr_main_navhost))
    }
}