package com.technishaun.openweather.objects.items.responsecityweather


import android.os.Parcel
import com.google.gson.annotations.SerializedName
import android.os.Parcelable

data class Temperature(
    @SerializedName("temp")
    val temp: Double,
    @SerializedName("temp_max")
    val tempMax: Double,
    @SerializedName("temp_min")
    val tempMin: Double
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readDouble(),
        source.readDouble(),
        source.readDouble()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeDouble(temp)
        writeDouble(tempMax)
        writeDouble(tempMin)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Temperature> = object : Parcelable.Creator<Temperature> {
            override fun createFromParcel(source: Parcel): Temperature = Temperature(source)
            override fun newArray(size: Int): Array<Temperature?> = arrayOfNulls(size)
        }
    }
}